<?php
namespace Billow\Utilities\Traits;

use Cache;
use Closure;

trait CachesThings
{
  public function cacheKey(string $for)
  {
    return sprintf(
      '%s/%s-%s:%s',
      $this->getTable(),
      $this->getKey(),
      $this->created_at->timestamp,
      $for
    );
  }

  public function cache(string $key, $value, $time = 60)
  {
    return Cache::put($this->cacheKey($key), $value, $time);
  }

  public function remember($key, Closure $callback, $time = 60)
  {
    return Cache::remember($this->cacheKey($key), $time, function () use ($callback) {
      return $callback();
    });
  }

}
