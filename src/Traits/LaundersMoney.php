<?php
namespace Billow\Utilities\Traits;

use Billow\Utilities\Money;
use Billow\Utilities\Observers\Launderer;

trait LaundersMoney
{
  public static function bootLaundersMoney()
  {
    static::observe(Launderer::class);
  }

  public function initMoney($value): Money
  {
    return is_int($value)
      ? new Money($value)
      : (new Money())->fromString($value);
  }

  public function asMoney($amount): float
  {
    return $this->initMoney($amount)->convertedAmount();
  }

  /**
   * @deprecated 14 June 2018
   * @see asMoney
   */
  public function formatMoney($amount)
  {
    return $this->asMoney($amount);
  }

  public function asCurrency($amount, $currency = null): string
  {
    return (new Money())->fromString($amount)->asCurrency(null, $currency);
  }

  public function asCents($amount): int
  {
    return $this->initMoney($amount)->amount();
  }

  public function getLaunderable(): array
  {
    return $this->launderable ?? [];
  }

  private function isLaunderable(string $key): bool
  {
    return property_exists(static::class, 'launderable') && in_array($key, $this->getLaunderable());
  }
}
