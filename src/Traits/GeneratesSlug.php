<?php
namespace Billow\Utilities\Traits;

use StringClean;

trait GeneratesSlug
{
  protected static function bootGeneratesSlug($column = 'name')
  {
    static::creating(function ($model) use ($column) {
      $model->slug = StringClean::slug($model->{$column}, $model);
    });
  }
}
