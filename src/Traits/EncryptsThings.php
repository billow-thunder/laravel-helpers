<?php
namespace Billow\Utilities\Traits;

use Billow\Utilities\Observers\Encrypter;

trait EncryptsThings
{
  public static function bootEncryptsThings()
  {
    static::observe(Encrypter::class);
  }

  public function getEncryptable(): array
  {
    return $this->encryptable;
  }

  public function encryptable(array $encryptable)
  {
    $this->encryptable = $encryptable;

    return $this;
  }

  private function isEncryptable(string $key): bool
  {
    return property_exists(static::class, 'encryptable') && in_array($key, $this->getEncryptable());
  }
}
