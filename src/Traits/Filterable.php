<?php
namespace Billow\Utilities\Traits;

use Billow\Utilities\QueryFilter;

trait Filterable
{
  public function scopeFilter($query, QueryFilter $filters)
  {
    return $filters->apply($query);
  }
}
