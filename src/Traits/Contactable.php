<?php
namespace Billow\Utilities\Traits;

use Billow\Utilities\Models\Contact;

trait Contactable
{
  public function contacts()
  {
    return $this->morphMany(Contact::class, 'contactable');
  }
}
