<?php
namespace Billow\Utilities\Traits;

trait FormattedAddress
{
  public function formatAddress(array $addressVars)
  {
    return collect($addressVars)
      ->reduce(function ($string, $var) {
        return "{$string}{$var}, ";
      }, '');
  }

}
