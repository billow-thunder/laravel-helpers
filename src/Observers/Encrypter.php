<?php
namespace Billow\Utilities\Observers;

use Illuminate\Database\Eloquent\Model;

class Encrypter extends ReadWriteObserver
{
  protected $write = 'encrypt';
  protected $read = 'decrypt';

  protected function decrypt(Model $model)
  {
    collect($model->getEncryptable())->each(function ($attribute) use ($model) {
      $value = $model->getAttribute($attribute);
      if ((bool) $value) {
        $model->setAttribute($attribute, decrypt($value));
      }
    });
  }

  protected function encrypt(Model $model)
  {
    collect($model->getEncryptable())->each(function ($attribute) use ($model) {
      $value = $model->getAttribute($attribute);
      if ((bool) $value) {
        $model->setAttribute($attribute, encrypt((string) $value));
      }
    });
  }
}
