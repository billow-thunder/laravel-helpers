<?php
namespace Billow\Utilities\Observers;

use Illuminate\Database\Eloquent\Model;

class ReadWriteObserver
{
  public function saving(Model $model)
  {
    $this->{$this->write}($model);
  }

  public function retrieved(Model $model)
  {
    $this->{$this->read}($model);
  }

  public function saved(Model $model)
  {
    $this->{$this->read}($model);
  }
}
