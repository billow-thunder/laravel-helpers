<?php
namespace Billow\Utilities\Observers;

use Illuminate\Database\Eloquent\Model;

class Launderer extends ReadWriteObserver
{
  protected $write = 'asCents';
  protected $read = 'asMoney';

  protected function asMoney(Model $model)
  {
    collect($model->getLaunderable())->each(function ($attribute) use ($model) {
      $model->setAttribute(
        $attribute,
        $model->asMoney($model->getAttribute($attribute))
      );
    });
  }

  protected function asCents(Model $model)
  {
    collect($model->getLaunderable())->each(function ($attribute) use ($model) {
      $model->setAttribute(
        $attribute,
        $model->asCents((string) $model->getAttribute($attribute))
      );
    });
  }
}
