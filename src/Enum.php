<?php
namespace Billow\Utilities;

use Illuminate\Support\Str;
use ReflectionClass;

abstract class Enum
{
  private static function getReflectedConstants(): array
  {
    return (new ReflectionClass(static::class))->getConstants();
  }

  private static function constants(): array
  {
    return array_flip(static::getReflectedConstants());
  }

  private static function values(): array
  {
    return static::getReflectedConstants();
  }

  public static function name($value)
  {
    $map = static::constants();
    return (array_key_exists($value, $map) ? $map[$value] : null);
  }

  public static function value($name)
  {
    $map = static::values();
    return array_key_exists($name, $map) ? $map[$name] : null;
  }

  public static function exists($value): bool
  {
    return array_key_exists($value, static::constants());
  }

  public static function displayName($value): string
  {
    return Str::titleSentence(static::name($value));
  }

  public static function payload($name, $key)
  {
    return [
      'value' => (int) $key,
      'name' => $name,
      'display_name' => Str::titleSentence($name),
    ];
  }

  public static function all(): array
  {
    return collect(static::constants())->map(function ($name, $key) {
      return static::payload($name, $key);
    })->values()->all();
  }
}
