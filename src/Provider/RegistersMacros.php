<?php
namespace Billow\Utilities\Provider;

use Illuminate\Support\Str;
use Illuminate\Database\Schema\Blueprint;

trait RegistersMacros
{
  private function registerMacros()
  {
    // Str::titleSentence('somethingNamedFoo') => "Something Named Foo"
    if (!Str::hasMacro('titleSentence')) {
      Str::macro('titleSentence', function (string $value) {
        return str_replace('_', ' ', Str::title(Str::snake($value)));
      });
    }

    // belongsTo(type = 'user', [table = null, [key = 'id']])
    if (!Blueprint::hasMacro('belongsTo')) {
      Blueprint::macro('belongsTo', function (string $type, string $table = null, $key = 'id') {
        $schema = $this->unsignedInteger("{$type}_{$key}")->index();

        $this->foreign("{$type}_{$key}")
          ->references($key)
          ->on($table ?? Str::plural($type));

        return $schema;
      });
    }

    // slug([column = 'slug])
    if (!Blueprint::hasMacro('slug')) {
      Blueprint::macro('slug', function (string $column = 'slug') {
        return $this->string($column)->unique()->index();
      });
    }

    // remarks|notes|comments([long = false])
    $remarkable = function (string $column) {
      return function ($long = false) use ($column) {
        $type = $long ? 'longText' : 'text';
        $this->$type($column)->nullable();
      };
    };

    collect(['remarks', 'notes', 'comments'])->each(function ($macro) use ($remarkable) {
      if (!Blueprint::hasMacro($macro)) {
        Blueprint::macro($macro, $remarkable($macro));
      }
    });
  }
}
