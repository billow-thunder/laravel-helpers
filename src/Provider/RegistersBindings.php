<?php
namespace Billow\Utilities\Provider;

use Billow\Utilities\{ Money, StringClean };

trait RegistersBindings
{
  private function registerBindings()
  {
    $this->app->bind('stringclean', function () {
      return new StringClean();
    });

    $this->app->bind('money', function () {
      return new Money();
    });
  }
}
