<?php
namespace Billow\Utilities;

use Illuminate\Support\Str;

class StringClean
{
	public function heading($string) {
		return ucwords(strtolower(trim($string)));
	}

	public function name($string) {
		return ucfirst(strtolower(trim($string)));
	}

	public function slug($string, $model, $workingColumn = 'name', $slugColumn = 'slug') {
		$slug = Str::slug($string);
		$count = $model::where($slugColumn, $slug)->count();
		if($count > 0) {
			$count = $count + 1;
			$slug = $slug .'-'. $count;
		}
		return $slug;
	}

	public function linesToBreaks($string)
	{
		return nl2br($string);
	}

	public function breaksToLines($string)
	{
		return preg_replace('#<br\s*/?>#i', "\n", $string);
	}
}