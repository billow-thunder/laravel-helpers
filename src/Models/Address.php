<?php
namespace Billow\Utilities\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
  use SoftDeletes;

  protected $fillable = [
    'addressable_id',
    'addressable_type',
    'building',
    'street',
    'suburb',
    'city',
    'province',
    'country',
    'post_code',
    'lat',
    'lng'
  ];

  protected $appends = [
    'print_address'
  ];

  public function addressable()
  {
      return $this->morphTo();
  }

  public function getPrintAddressAttribute()
  {
    return substr(
      collect([
        $this->building,
        $this->street,
        $this->suburb,
        $this->city,
        $this->province,
        $this->country,
        $this->post_code,
      ])->reduce(function($string, $item) {
        return (strlen($item) > 0) ? "{$string}$item, " : $string;
      }, ''),
      0,
      -2
    );
  }
}
