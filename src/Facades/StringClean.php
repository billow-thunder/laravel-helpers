<?php
namespace Billow\Utilities\Facades;

use Illuminate\Support\Facades\Facade;

class StringClean extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'stringclean';
  }
}
