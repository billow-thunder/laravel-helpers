# Laravel Helpers, by Billow

[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/does-not-contain-treenuts.svg)](https://forthebadge.com)

## Installation

```bash
composer require billowapp/laravel-helpers
```

If your version of Laravel doesn’t support package auto-discovery, add the provider to `config/app.php`:

```php
Billow\Utilities\BillowHelperProvider::class
```

If you’d like to access the StringClean Facade as an Alias, add this too:

```php
'StringClean' => Billow\Utilities\Facades\StringClean::class
```

## Model Traits

Laravel Helpers comes with some useful model traits that you can use to manipulate attributes on the fly, wither using helper methods, or observer-initialisers.

### LaundersMoney

This trait provides simple helpers to work with the Money class, included in `billowapp/show-me-the-money` (installed via this package).

```php
use Billow\Utilities\Traits\LaundersMoney;
```

**Available helpers:**

**`asCents($amount): int`** Returns your money in cents. Because coins are cool. Example: `50` is stored as `5000`.

**`asMoney($amount): float`** Floats your cents back to money. Example: `5000` is stored as `50`.

**`asCurrency($amount): float`** Uses a formatter to format your cents as currency. Example `5000` => `R 50.00`

**Access & Mutate:**

To use these, simply use the applicable accessor and mutator methods for the concerned attributes:

```php
public function setPriceAttribute($price)
{
  $this->attributes['price'] = $this->asCents((string) $price);
}

public function getPriceAttribute($price)
{
  return $this->asMoney($price);
}
```

Or, you can use the built-in observer-initialisers, via the `$launderable` class-property:

```php
protected $launderable = [
  'price'
];
```

### EncryptsThings

This trait can only be initialised via observer-initialisers, as the encrypt/decrypt helpers are already available to you, so no extra effort is required to access them in a clean way.

To enable on-the-fly encryption in your model:

```php
use Billow\Utilities\Traits\EncryptsThings;

…

protected $encryptable = [
  'id_number',
  'passport_number'
];
```
